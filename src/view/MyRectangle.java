package view;

import java.awt.Dimension;
import java.awt.Point;

public class MyRectangle {

	private Point point;
	private Dimension dimension;
	private String name;

	public MyRectangle(Point point, Dimension dimension, String name) {		
		this.point = point;	
		this.dimension = dimension;
		this.name = name;
	}

	public Point getPoint() {
		return point;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public String getName() {
		return name;
	}
	
	public Point getCenterPoint() {
		return new Point(
				this.point.x - this.dimension.width / 2,
				this.point.y - this.dimension.width / 2
				);
	}

	
}
