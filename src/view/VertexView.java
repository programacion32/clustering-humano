package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JPanel;

public class VertexView extends JPanel {

	private ArrayList<MyRectangle> rect;
	private ArrayList<MyLineConection> lineConection;
	private Dimension dimRect;
	private Dimension dimension;

	public VertexView(Dimension dimension) {
		// TODO Auto-generated constructor stub
		rect = new ArrayList<>();
		lineConection = new ArrayList<>();
		this.dimension = dimension;
		this.dimRect = new Dimension(20, 20);
	}

	public void addVertexView(Point point, String name) {
		MyRectangle rect = new MyRectangle(point, this.dimension, name);
		this.rect.add(rect);
	}

	public void buildConnections() {
		for (MyRectangle rect1 : this.rect) {
			for (MyRectangle rect2 : this.rect) {
				if (rect1.getPoint() != rect2.getPoint())
					this.lineConection.add(new MyLineConection(rect1, rect2));
			}
		}
	}

	public Dimension getPreferredSize() {
		return this.dimension;
	}

	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawBackground(g);
		for (MyRectangle rect1 : this.rect) {
			for (MyRectangle rect2 : this.rect) {
				if (rect1.getPoint() != rect2.getPoint()) {
					drawRect(g, rect1);
					drawRect(g, rect2);
					drawLine(g, rect1, rect2);
				}
			}
		}		
	}
	
	private void drawBackground(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.dimension.width, this.dimension.height);		
		g.drawRect(0, 0, this.dimension.width-1, this.dimension.height-6);
	}

	private void drawLine(Graphics g, MyRectangle rect1, MyRectangle rect2) {
		int width = (int) this.dimRect.getWidth() / 2;
		g.drawLine(
				rect1.getPoint().x + width,
				rect1.getPoint().y + width,
				rect2.getPoint().x + width,
				rect2.getPoint().y + width);
	}

	private void drawRect(Graphics g, MyRectangle rect) {
		g.drawString(rect.getName(), rect.getPoint().x, rect.getPoint().y - 10);
		g.setColor(Color.RED);
		g.fillRect(rect.getPoint().x, rect.getPoint().y, this.dimRect.width, this.dimRect.height);
		g.setColor(Color.BLACK);
		g.drawRect(rect.getPoint().x, rect.getPoint().y, this.dimRect.width, this.dimRect.height);
	}
}
