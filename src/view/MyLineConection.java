package view;

public class MyLineConection {
	private MyRectangle rect1;
	private MyRectangle rect2;

	public MyLineConection(MyRectangle rect1, MyRectangle rect2) {
		this.rect1 = rect1;
		this.rect2 = rect2;
	}

	public MyRectangle getRect1() {
		return rect1;
	}

	public MyRectangle getRect2() {
		return rect2;
	}

}
