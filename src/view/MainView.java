/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Random;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import model.Graph;
import model.Kruskal;
import model.Person;
import model.Vertex;

/**
 *
 * @author Tempu
 */
public class MainView extends javax.swing.JFrame {

    /**
     * Creates new form MainView
     */
    private Graph<Person> graph;
    private Kruskal<Person> kruskal;

    private Graph<MapMarker> graphPoints;
    private Kruskal<MapMarker> kruskalPoints;

    public MainView() {
        initVariables();
        initComponents();
        this.setVisible(true);
    }

    private void initVariables() {
        this.graph = new Graph<>();
        this.graphPoints = new Graph<>();
    }

    private void addPerson() {
        String name = jTextFieldPersonName.getText();
        int sports = Integer.parseInt(jTextFieldPersonSports.getText());
        int music = Integer.parseInt(jTextFieldPersonMusic.getText());
        int shows = Integer.parseInt(jTextFieldPersonShows.getText());
        int science = Integer.parseInt(jTextFieldPersonScience.getText());
        Person person = new Person(name, sports, music, shows, science);
        addPerson(person);
    }

    private void addPerson(Person p) {
        this.graph.addVertex(p);
        addMarker(p.name);
        textPanelVertex();
    }

    // Cruza todos los vertices entre si, y obtiene a la persona, luego ejecuta y
    // busca compatibilidad
    private void buildConnections() {
        for (int v1 : this.graph.getAllVertex().keySet()) {
            for (int v2 : this.graph.getAllVertex().keySet()) {
                if (v1 != v2) {
                    // obtengo compatibilidad entre personas
                    int compatibility = this.graph.getAllVertex().get(v1).getElement()
                            .getCompatibility(this.graph.getAllVertex().get(v2).getElement());

                    // cargo el enlace entre personas y le agrego el peso
                    this.graph.addConnectionWithWeight(this.graph.getAllVertex().get(v1),
                            this.graph.getAllVertex().get(v2), compatibility);

                    // cargo el enlace entre puntos y le agrego el peso
                    this.graphPoints.addConnectionWithWeight(this.graphPoints.getAllVertex().get(v1),
                            this.graphPoints.getAllVertex().get(v2), compatibility);
                }
            }
        }
    }

    private void deletePerson() {
        String name = jTextFieldPersonName.getText();
        Iterator<Integer> it = this.graph.getAllVertex().keySet().iterator();
        while (it.hasNext()) {
            int key = (int) it.next();
            if (this.graph.getVertexIndex(key).getElement().name.equals(name)) {
                it.remove();
                this.graphPoints.removeVertex(this.graphPoints.getAllVertex().get(key));
            }
        }
        textPanelVertex();
        mapMarkerPanelVertex();
    }

    private void deleteFields() {
        jTextFieldPersonName.setText("");
        jTextFieldPersonSports.setText("");
        jTextFieldPersonMusic.setText("");
        jTextFieldPersonShows.setText("");
        jTextFieldPersonScience.setText("");
    }

    private void textPanelVertex() {
        String string = "";
        for (Integer v : this.graph.getAllVertex().keySet()) {
            string += v + ":" + this.graph.getVertexIndex(v).getElement().name + ", ";
        }
        this.jTextPaneVertex.setText(string);
    }

    private void textPanelConnections() {
        textPanelVertex();
        String string = "";
        for (String v : this.graph.getAllconnections().keySet()) {
            string += v + " : " + this.graph.getAllconnectionsWithWeight().get(v).toString() + " | ";
        }
        this.jTextPaneConnections.setText(string);
        mapMarkerPanelConnections();
    }

    private void mapMarkerPanelVertex() {
        this.jmapv.removeAllMapMarkers();
        for (Integer v : this.graphPoints.getAllVertex().keySet()) {
            this.jmapv.addMapMarker(this.graphPoints.getVertexIndex(v).getElement());
        }
    }

    // dibuja las lineas entre las conexiones
    private void mapMarkerPanelConnections() {
        this.jmapv.removeAllMapPolygons();
        mapMarkerPanelVertex();
        for (String v : this.graphPoints.getAllconnections().keySet()) {
            Set<Vertex<MapMarker>> points = this.graphPoints.getAllconnections().get(v).getAristas();
            ArrayList<Coordinate> route = new ArrayList();            
            points.forEach(vm -> {
                route.add(vm.getElement().getCoordinate());
            });
            route.add(route.get(1));            
            this.jmapv.addMapPolygon(new MapPolygonImpl(route));
        }
    }

    private void addMarker(String name) {
        Random r = new Random();
        float x = r.nextFloat() * 3;
        float y = r.nextFloat() * 3;
        Coordinate c = new Coordinate(x, y);
        MapMarker m = new MapMarkerDot(name, c);
        m.getStyle().setBackColor(Color.RED);
        m.getStyle().setColor(Color.BLACK);
        this.graphPoints.addVertex(m);
        this.jmapv.addMapMarker(m);
        this.jmapv.setDisplayPosition(c, 6);
    }

    private void jmapvInit() {
        this.jmapv.setZoomControlsVisible(false);
        this.jmapv.setDisplayPosition(new Coordinate(0, 0), 6);
    }

    private void buildMinimumTree() {
        this.kruskal = new Kruskal<Person>(graph);
        this.graph = kruskal.minimumTreeGenerator();

        this.kruskalPoints = new Kruskal<MapMarker>(graphPoints);
        this.graphPoints = kruskalPoints.minimumTreeGenerator();
    }

    private void buildMaximumTree() {
        this.kruskal = new Kruskal<Person>(graph);
        this.graph = kruskal.maximumTreeGenerator();

        this.kruskalPoints = new Kruskal<MapMarker>(graphPoints);
        this.graphPoints = kruskalPoints.maximumTreeGenerator();
    }

    private void chargeExample() {
        Person A = new Person("A", 2, 3, 1, 5);
        Person B = new Person("B", 5, 5, 1, 1);
        Person C = new Person("C", 5, 5, 1, 1);
        Person D = new Person("D", 3, 5, 4, 2);
        Person E = new Person("E", 3, 3, 3, 3);

        addPerson(A);
        addPerson(B);
        addPerson(C);
        addPerson(D);
        addPerson(E);
    }

    private void clustering() {
        int val = Integer.parseInt(jSpinnerClusterFactor.getValue().toString());

        this.kruskal = new Kruskal<>(graph);
        this.graph = kruskal.clustering(val);

        this.kruskalPoints = new Kruskal<>(graphPoints);
        this.graphPoints = kruskalPoints.clustering(val);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
    	
    	jmapv = new JMapViewer();
        jPanelMain = new javax.swing.JPanel();
        jPanelPerson = new javax.swing.JPanel();
        jLabelTitle = new javax.swing.JLabel();
        jLabelPersonName = new javax.swing.JLabel();
        jLabelPersonSports = new javax.swing.JLabel();
        jLabelPersonMusic = new javax.swing.JLabel();
        jLabelPersonShows = new javax.swing.JLabel();
        jLabelPersonScience = new javax.swing.JLabel();
        jTextFieldPersonName = new javax.swing.JTextField();
        jTextFieldPersonSports = new javax.swing.JTextField();
        jTextFieldPersonMusic = new javax.swing.JTextField();
        jTextFieldPersonShows = new javax.swing.JTextField();
        jTextFieldPersonScience = new javax.swing.JTextField();
        jButtonPersonAdd = new javax.swing.JButton();
        jButtonPersonCancel = new javax.swing.JButton();
        jButtonPersonRemove = new javax.swing.JButton();
        jButtonPersonLoad = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jButtonKruskalMinimum = new javax.swing.JButton();
        jButtonBuildGraph = new javax.swing.JButton();
        jButtonKruskalMaximum = new javax.swing.JButton();
        jButtonClustering = new javax.swing.JButton();
        jSpinnerClusterFactor = new javax.swing.JSpinner();
        jPanelGraph = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabelVertex = new javax.swing.JLabel();
        jLabelConnections = new javax.swing.JLabel();
        jTextPaneVertex = new javax.swing.JTextPane();
        jTextPaneConnections = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanelMain.setBackground(new java.awt.Color(153, 153, 153));

        jPanelPerson.setBackground(new java.awt.Color(204, 204, 204));

        jLabelTitle.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabelTitle.setForeground(new java.awt.Color(0, 0, 0));
        jLabelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTitle.setText("Persona");

        jLabelPersonName.setForeground(new java.awt.Color(0, 0, 0));
        jLabelPersonName.setText("Nombre:");

        jLabelPersonSports.setForeground(new java.awt.Color(0, 0, 0));
        jLabelPersonSports.setText("Deportes:");

        jLabelPersonMusic.setForeground(new java.awt.Color(0, 0, 0));
        jLabelPersonMusic.setText("Musica:");

        jLabelPersonShows.setForeground(new java.awt.Color(0, 0, 0));
        jLabelPersonShows.setText("Espectaculos:");

        jLabelPersonScience.setForeground(new java.awt.Color(0, 0, 0));
        jLabelPersonScience.setText("Ciencia:");

        jButtonPersonAdd.setText("Agregar");
        jButtonPersonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPersonAddActionPerformed(evt);
            }
        });

        jButtonPersonCancel.setText("Cancelar");
        jButtonPersonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPersonCancelActionPerformed(evt);
            }
        });

        jButtonPersonRemove.setText("Borrar");
        jButtonPersonRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPersonRemoveActionPerformed(evt);
            }
        });

        jButtonPersonLoad.setText("Cargar Ejemplo");
        jButtonPersonLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPersonLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPersonLayout = new javax.swing.GroupLayout(jPanelPerson);
        jPanelPerson.setLayout(jPanelPersonLayout);
        jPanelPersonLayout.setHorizontalGroup(
            jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPersonLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPersonLayout.createSequentialGroup()
                        .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelPersonName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPersonMusic, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPersonSports, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPersonShows, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPersonScience, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPersonLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jTextFieldPersonName, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelPersonLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextFieldPersonMusic)
                                    .addComponent(jTextFieldPersonSports, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldPersonScience, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextFieldPersonShows))))
                        .addGap(20, 20, 20))
                    .addGroup(jPanelPersonLayout.createSequentialGroup()
                        .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelPersonLayout.createSequentialGroup()
                                .addComponent(jButtonPersonAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonPersonCancel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonPersonRemove))
                            .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelPersonLayout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addComponent(jButtonPersonLoad)))
                        .addGap(18, 18, 18))))
        );
        jPanelPersonLayout.setVerticalGroup(
            jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPersonLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabelTitle)
                .addGap(18, 18, 18)
                .addComponent(jButtonPersonLoad)
                .addGap(38, 38, 38)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonName)
                    .addComponent(jTextFieldPersonName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonSports)
                    .addComponent(jTextFieldPersonSports, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonMusic)
                    .addComponent(jTextFieldPersonMusic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonShows)
                    .addComponent(jTextFieldPersonShows, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPersonScience)
                    .addComponent(jTextFieldPersonScience, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(jPanelPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonPersonAdd)
                    .addComponent(jButtonPersonCancel)
                    .addComponent(jButtonPersonRemove))
                .addGap(51, 51, 51))
        );

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jButtonKruskalMinimum.setText("Arbol Minimo");
        jButtonKruskalMinimum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonKruskalMinimumActionPerformed(evt);
            }
        });

        jButtonBuildGraph.setText("Generar Grafo");
        jButtonBuildGraph.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBuildGraphActionPerformed(evt);
            }
        });

        jButtonKruskalMaximum.setText("Arbol Maximo");
        jButtonKruskalMaximum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonKruskalMaximumActionPerformed(evt);
            }
        });

        jButtonClustering.setText("Clusterizar");
        jButtonClustering.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClusteringActionPerformed(evt);
            }
        });

        jSpinnerClusterFactor.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jSpinnerClusterFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonClustering, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonKruskalMinimum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonKruskalMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(jButtonBuildGraph, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jButtonBuildGraph, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonKruskalMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonKruskalMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonClustering, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinnerClusterFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanelGraph.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanelGraphLayout = new javax.swing.GroupLayout(jPanelGraph);
        jPanelGraph.setLayout(jPanelGraphLayout);
        jPanelGraphLayout.setHorizontalGroup(
            jPanelGraphLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 580, Short.MAX_VALUE)
            .addComponent(jmapv,0, 580, Short.MAX_VALUE)
        );
        jPanelGraphLayout.setVerticalGroup(
            jPanelGraphLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addComponent(jmapv,0, 0, Short.MAX_VALUE)
        );
        jmapvInit();

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));

        jLabelVertex.setForeground(new java.awt.Color(0, 0, 0));
        jLabelVertex.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelVertex.setText("Vertices");
        jLabelVertex.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelConnections.setForeground(new java.awt.Color(0, 0, 0));
        jLabelConnections.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelConnections.setText("Aristas");
        jLabelConnections.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextPaneVertex.setEditable(false);

        jTextPaneConnections.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelVertex, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addComponent(jLabelConnections, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextPaneVertex, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextPaneConnections, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jLabelVertex, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTextPaneVertex, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabelConnections, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextPaneConnections, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout jPanelMainLayout = new javax.swing.GroupLayout(jPanelMain);
        jPanelMain.setLayout(jPanelMainLayout);
        jPanelMainLayout.setHorizontalGroup(
            jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelPerson, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelMainLayout.setVerticalGroup(
            jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelPerson, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonPersonAddActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonPersonAddActionPerformed
        addPerson();
        deleteFields();
    }// GEN-LAST:event_jButtonPersonAddActionPerformed

    private void jButtonPersonCancelActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonPersonCancelActionPerformed
        deleteFields();
    }// GEN-LAST:event_jButtonPersonCancelActionPerformed

    private void jButtonPersonRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonPersonRemoveActionPerformed
        deletePerson();
        deleteFields();
    }// GEN-LAST:event_jButtonPersonRemoveActionPerformed

    private void jButtonBuildGraphActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonBuildGraphActionPerformed
        buildConnections();
        textPanelConnections();
    }// GEN-LAST:event_jButtonBuildGraphActionPerformed

    private void jButtonKruskalMaximumActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonKruskalMaximumActionPerformed
        buildMaximumTree();
        textPanelConnections();
    }// GEN-LAST:event_jButtonKruskalMaximumActionPerformed

    private void jButtonKruskalMinimumActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonKruskalMinimumActionPerformed
        buildMinimumTree();
        textPanelConnections();
    }// GEN-LAST:event_jButtonKruskalMinimumActionPerformed

    private void jButtonPersonLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonPersonLoadActionPerformed
        chargeExample();
        textPanelVertex();
    }// GEN-LAST:event_jButtonPersonLoadActionPerformed

    private void jButtonClusteringActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButtonClusteringActionPerformed
        clustering();
        textPanelConnections();
    }// GEN-LAST:event_jButtonClusteringActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBuildGraph;
    private javax.swing.JButton jButtonClustering;
    private javax.swing.JButton jButtonKruskalMaximum;
    private javax.swing.JButton jButtonKruskalMinimum;
    private javax.swing.JButton jButtonPersonAdd;
    private javax.swing.JButton jButtonPersonCancel;
    private javax.swing.JButton jButtonPersonLoad;
    private javax.swing.JButton jButtonPersonRemove;
    private javax.swing.JLabel jLabelConnections;
    private javax.swing.JLabel jLabelPersonMusic;
    private javax.swing.JLabel jLabelPersonName;
    private javax.swing.JLabel jLabelPersonScience;
    private javax.swing.JLabel jLabelPersonShows;
    private javax.swing.JLabel jLabelPersonSports;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JLabel jLabelVertex;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelGraph;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelPerson;
    private javax.swing.JSpinner jSpinnerClusterFactor;
    private javax.swing.JTextField jTextFieldPersonMusic;
    private javax.swing.JTextField jTextFieldPersonName;
    private javax.swing.JTextField jTextFieldPersonScience;
    private javax.swing.JTextField jTextFieldPersonShows;
    private javax.swing.JTextField jTextFieldPersonSports;
    private javax.swing.JTextPane jTextPaneConnections;
    private javax.swing.JTextPane jTextPaneVertex;
    private JMapViewer jmapv;
    // End of variables declaration//GEN-END:variables
}
