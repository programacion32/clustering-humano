package model;

import java.util.HashMap;
import java.util.Map;

public class Kruskal<E> {

	private Graph<E> G;
	private BFS<E> bfs;
	private Map<String, Integer> connectionsWithWeight;
	private Map<String, String> connections;

	public Kruskal(Graph<E> g) {
		this.G = g;
		this.bfs = new BFS<>();
		this.connectionsWithWeight = g.getAllconnectionsWithWeight();
		this.connections = mapControl(this.connectionsWithWeight);
	}

	public Graph<E> minimumTreeGenerator() {
		return treeGenerator(true);
	}

	public Graph<E> maximumTreeGenerator() {
		return treeGenerator(false);
	}

	private Graph<E> treeGenerator(boolean weightControl) {
		Graph<E> graph = new Graph<>();
		while (this.connections.size() != 0) {
			String indexConnection = weightControl ? getMinimumWeightIndex() : getMaximumWeightIndex();
			int k1 = Integer.parseInt(indexConnection.substring(0, 1));
			int k2 = Integer.parseInt(indexConnection.substring(1, 2));

			if (!bfs.reachable(graph, k1).contains(k2)) {
				Vertex<E> v1 = this.G.getVertexIndex(k1);
				Vertex<E> v2 = this.G.getVertexIndex(k2);
				graph.addVertex(v1);
				graph.addVertex(v2);
				graph.addConnectionWithWeight(v1, v2, this.connectionsWithWeight.get(indexConnection));
			}
			this.connections.remove(indexConnection);
			this.connectionsWithWeight.remove(indexConnection);
		}
		return graph;
	}

	private String getMinimumWeightIndex() {
		int weight = this.G.getTotalWeight();
		String indexKeyWeight = "";
		for (String key : this.connectionsWithWeight.keySet()) {
			if (weight > this.connectionsWithWeight.get(key)) {
				weight = this.connectionsWithWeight.get(key);
				indexKeyWeight = key;
			}
		}
		return indexKeyWeight;
	}

	private String getMaximumWeightIndex() {
		int weight = -1;
		String indexKeyWeight = "";
		for (String key : this.connectionsWithWeight.keySet()) {
			if (weight < this.connectionsWithWeight.get(key)) {
				weight = this.connectionsWithWeight.get(key);
				indexKeyWeight = key;
			}
		}
		return indexKeyWeight;
	}

	public Graph<E> clustering(int clusterFactor) {
		String indexCut;
		for (int i = 0; i < clusterFactor; i++) {
			indexCut = getMaximumWeightIndex();
			if (indexCut != null) {
				this.G.removeConnectionIndex(indexCut);
				this.connections.remove(indexCut);
				this.connectionsWithWeight.remove(indexCut);
			}
		}
		return this.G;
	}

	// convierte un map <string, integer> en <string, string
	private Map<String, String> mapControl(Map<String, Integer> m) {
		Map<String, String> control = new HashMap<>();
		for (String s : m.keySet()) {
			control.put(s, s);
		}
		return control;
	}
}
