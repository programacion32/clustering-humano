package model;

public class Person {
	public String name;
	public int sports;
	public int music;
	public int shows;
	public int sciences;

	public Person(String name, int sports, int music, int shows, int sciences) {
		this.name = name;
		this.sports = sports;
		this.music = music;
		this.shows = shows;
		this.sciences = sciences;
	}

	private void absConvert() {
		this.sports = Math.abs(sports);
		this.music = Math.abs(music);
		this.shows = Math.abs(shows);
		this.sciences = Math.abs(sciences);
	}

	public int getCompatibility(Person p) {
		this.absConvert();
		p.absConvert();
		return Math.abs(sports - p.sports) + Math.abs(music - p.music) + Math.abs(shows - p.shows)
				+ Math.abs(sciences - p.sciences);

	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("{");
		s.append("\"name\":" + "\"" + name + "\"");
		s.append(",\"sports\":" + sports);
		s.append(",\"music\":" + music);
		s.append(",\"shows\":" + shows);
		s.append(",\"sciences\":" + sciences);
		s.append("}");
		return s.toString();
	}
}
