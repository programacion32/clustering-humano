package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS<E> {

	public boolean isConvex(Graph<E> g) {

		if (g == null) {
			throw new IllegalArgumentException();
		}

		if (g.getSize() == 0) {
			return false;
		}

		return reachable(g, 0).size() == g.getSize();
	}

	public Set<Integer> reachable(Graph<E> g, int vertIndex) {
		Set<Integer> marked = new HashSet<Integer>();
		ArrayList<Integer> pending = new ArrayList<Integer>();
		pending.add(vertIndex);

		while (pending.size() != 0 && g.getSize() != 0) {
			int actual = pending.get(0);
			marked.add(actual);
			pending.remove(0);

			for (int v : g.neivers(actual)) {
				if (marked.contains(v) == false) {
					pending.add(v);
				}
			}
		}
		return marked;
	}
}
