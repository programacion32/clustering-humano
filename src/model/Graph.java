package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Graph<E> {

    private Map<String, Connection<Vertex<E>>> connections;
    private Map<String, Integer> connectionsWeight;
    private Map<Integer, Vertex<E>> vertex;
    private int indexControl;
    private int totalWeight;

    public Graph() {
        this.connections = new HashMap<String, Connection<Vertex<E>>>();
        this.connectionsWeight = new HashMap<String, Integer>();
        this.vertex = new HashMap<Integer, Vertex<E>>();
        this.indexControl = 0;
        this.totalWeight = 0;
    }

    public int getSize() {
        return this.vertex.size();
    }

    public void addVertex(E element) {
        this.indexControl++;
        this.vertex.put(this.indexControl, new Vertex<E>(element, this.indexControl));
    }

    public void addVertex(Vertex<E> v) {
        this.vertex.put(v.getIndex(), v);
    }

    public void removeVertex(Vertex<E> v) {
        existVertex(v);
        removeVertexConnection(v);
        this.vertex.remove(v.getIndex());
    }

    public boolean existVertexIndex(int vertIndex) {
        checkPositiveIndex(vertIndex);
        return vertex.containsKey(vertIndex);

    }

    public boolean existVertex(Vertex<E> v) {
        if (!vertex.containsKey(v.getIndex())) {
            throw new IllegalArgumentException("No existe vertice: " + v.getIndex());
        }
        return true;
    }

    public void checkVertexNotLoop(Vertex<E> v1, Vertex<E> v2) {
        if (v1.getIndex() == v2.getIndex()) {
            throw new IllegalArgumentException("Vertices con mismo indice: " + v1.getIndex() + " - " + v2.getIndex());
        }
    }

    public void addConnection(Vertex<E> v1, Vertex<E> v2) {
        existVertex(v1);
        existVertex(v2);
        checkVertexNotLoop(v1, v2);
        Connection<Vertex<E>> cTemp = new Connection<Vertex<E>>(v1, v2);
        String key1 = Integer.toString(v1.getIndex()) + Integer.toString(v2.getIndex());
        connections.put(key1, cTemp);
    }

    public void addConnectionWithWeight(Vertex<E> v1, Vertex<E> v2, int weight) {
        existVertex(v1);
        existVertex(v2);
        checkVertexNotLoop(v1, v2);
        Connection<Vertex<E>> cTemp = new Connection<Vertex<E>>(v1, v2);
        cTemp.setWeight(weight);
        this.totalWeight += weight;
        String key1 = Integer.toString(v1.getIndex()) + Integer.toString(v2.getIndex());
        connections.put(key1, cTemp);
        connectionsWeight.put(key1, weight);
    }

    public Set<Vertex<E>> getConnection(Vertex<E> v1, Vertex<E> v2) {
        if (existConection(v1, v2)) {
            String key1 = Integer.toString(v1.getIndex()) + Integer.toString(v2.getIndex());
            return this.connections.get(key1).getAristas();
        }
        return null;
    }

    public int getConnectionWeight(Vertex<E> v1, Vertex<E> v2) {
        if (existConection(v1, v2)) {
            String key1 = Integer.toString(v1.getIndex()) + Integer.toString(v2.getIndex());
            return this.connections.get(key1).getWeight();
        }
        return -1;
    }

    public boolean existConection(Vertex<E> v1, Vertex<E> v2) {
        String key1 = Integer.toString(v1.getIndex()) + Integer.toString(v2.getIndex());
        String key2 = Integer.toString(v2.getIndex()) + Integer.toString(v1.getIndex());
        return this.connections.containsKey(key1) || this.connections.containsKey(key2);
    }

    public boolean removeConnection(Vertex<E> v1, Vertex<E> v2) {
        if (existConection(v1, v2)) {
            String key1 = Integer.toString(v1.getIndex()) + Integer.toString(v2.getIndex());
            String key2 = Integer.toString(v2.getIndex()) + Integer.toString(v1.getIndex());
            connections.remove(key1);
            connections.remove(key2);
            connectionsWeight.remove(key1);
            connectionsWeight.remove(key2);
            return true;
        }
        return false;
    }

    public boolean existConection(String key) {
        return this.connections.containsKey(key);
    }

    public boolean removeConnectionIndex(String conecIndex) {
        if (existConection(conecIndex)) {
            String v1 = conecIndex.substring(0, 1);
            String v2 = conecIndex.substring(1, 2);
            String key1 = v1 + v2;
            String key2 = v2 + v1;
            connections.remove(key1);
            connections.remove(key2);
            connectionsWeight.remove(key1);
            connectionsWeight.remove(key2);
            return true;
        }
        return false;
    }

    // Recorro el conjunto de keys
    // Si el key esta en el conjunto, consigo su pareja
    // fracciono el id y me quedo con la pareja, luego devuelvo el conjunto de
    // parejas
    public Set<Integer> neivers(int vertIndex) {
        checkPositiveIndex(vertIndex);
        existVertexIndex(vertIndex);

        Set<Integer> vertexs = new HashSet<Integer>();
        for (String key : connections.keySet()) {
            if (key.contains(Integer.toString(vertIndex))) {
                String subKeyIndex = key.replace(Integer.toString(vertIndex), "");
                int subIndex = Integer.parseInt(subKeyIndex);
                vertexs.add(subIndex);
            }
        }
        return vertexs;
    }

    public int getTotalWeight() {
        return this.totalWeight;
    }

    public Map<String, Integer> getAllconnectionsWithWeight() {
        return this.connectionsWeight;
    }

    public Map<String, Connection<Vertex<E>>> getAllconnections() {
        return this.connections;
    }

    public Map<Integer, Vertex<E>> getAllVertex() {
        return this.vertex;
    }

    public Vertex<E> getVertexIndex(int vertIndex) {
        return this.vertex.get(vertIndex);
    }

    /// Private
    private void discountTotalWeight(int weight) {
        if (weight > 0) {
            this.totalWeight -= weight;
        }
    }

    private void checkVertexNotNullIndex(Vertex<E> e) {
        if (e == null) {
            throw new IllegalArgumentException("Se intenta cargar un elemento nulo: " + e);
        }
    }

    private void checkPositiveIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Index negativo: " + index);
        }
    }

    private void removeVertexConnection(Vertex<E> v1) {
        existVertex(v1);
        String serchedKey = Integer.toString(v1.getIndex());
        Set<String> keys = connections.keySet();
        Iterator<String> it = keys.iterator();
        while (it.hasNext()) {
            String keyIT = (String) it.next();
            if (keyIT.contains(serchedKey)) {
                discountTotalWeight(connections.get(keyIT).getWeight());
                it.remove();
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(connectionsWeight.toString());
        return s.toString();
    }

}
