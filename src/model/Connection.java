package model;

import java.util.HashSet;
import java.util.Set;

public class Connection<E> {
	private Set<E> setConnection;	
	private int weight;

	public Connection(E element1, E element2) {
		this.setConnection = new HashSet<E>();
		this.setConnection.add(element1);
		this.setConnection.add(element2);
		this.weight = -1;
	}

	public Set<E> getAristas() {
		return this.setConnection;
	}

	public boolean containElement(E elem) {
		return this.setConnection.contains(elem);
	}

	public boolean containConnection(Set<E> elem) {
		return this.setConnection.containsAll(elem);
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public int getWeight() {
		return this.weight;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();                
		s.append(setConnection.toString());
                s.append("[");
                s.append(this.weight);
		s.append("]");
		return s.toString();
	}

}
