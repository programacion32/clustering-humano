package model;

public class Vertex<E> {

	private E element;	
	private int numIndex;

	public Vertex(int numIndex) {
		checkVertex(numIndex);
		this.numIndex = numIndex;
	}

	public Vertex(E element, int numIndex) {
		checkVertex(numIndex);
		this.element = element;
		this.numIndex = numIndex;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;

		@SuppressWarnings("unchecked")
		Vertex<E> obj = (Vertex<E>) other;

		return this.numIndex == obj.numIndex;
	}

	public E getElement() {
		return this.element;
	}

	public int getIndex() {
		return this.numIndex;
	}
	
	public void setIndex(int numIndex) {
		this.numIndex = numIndex;
	}

	private void checkVertex(int index) {
		if (index < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + index);
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(" ");
		s.append(this.numIndex);
		return s.toString();
	}

}
