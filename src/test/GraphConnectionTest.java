package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Graph;
import model.Vertex;

public class GraphConnectionTest {

	@Test
	public void addConnectionTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);

		g.addVertex(v1);
		g.addVertex(v2);
		g.addConnection(v1, v2);

		assertTrue(g.existConection(v1, v2));
	}

	@Test
	public void addConnectionWithWeightTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);

		g.addVertex(v1);
		g.addVertex(v2);

		g.addConnectionWithWeight(v1, v2, 5);

		assertTrue(g.getConnectionWeight(v1, v2) == 5);
	}

	@Test
	public void removeConnectionTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);

		g.addVertex(v1);
		g.addVertex(v2);
		g.addConnection(v1, v2);
		g.removeConnection(v1, v2);

		assertTrue(!g.existConection(v1, v2));
	}

	@Test
	public void existConnectionTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);

		g.addVertex(v1);
		g.addVertex(v2);

		g.addConnection(v1, v2);

		assertTrue(g.existConection(v1, v2));
	}

	@Test
	public void notExistConnectionTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);

		g.addVertex(v1);
		g.addVertex(v2);

		assertTrue(!g.existConection(v1, v2));
	}	

}
