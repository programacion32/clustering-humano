package test;

import org.junit.Test;

import model.Graph;
import model.Vertex;

public class GraphNeiverTest {

	@Test
	public void twoNeiversTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);
		Vertex<Integer> v3 = new Vertex<Integer>(3);

		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);

		g.addConnection(v1, v2);
		g.addConnection(v1, v3);
		g.addConnection(v2, v3);

		int expected[] = { 2, 3 };

		Asserts.equals(expected, g.neivers(v1.getIndex()));
	}

	@Test
	public void threeNeiversTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);
		Vertex<Integer> v3 = new Vertex<Integer>(3);
		Vertex<Integer> v4 = new Vertex<Integer>(4);

		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v4);

		g.addConnection(v1, v2);
		g.addConnection(v1, v3);
		g.addConnection(v2, v3);
		g.addConnection(v1, v4);

		int expected[] = { 2, 3, 4 };

		Asserts.equals(expected, g.neivers(v1.getIndex()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void negativeVertexTest() {
		Graph<Integer> g = new Graph<Integer>();
		g.neivers(-1);
	}

}
