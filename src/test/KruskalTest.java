package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import model.Graph;
import model.Kruskal;
import model.Vertex;

public class KruskalTest {

	Graph<String> graph;

	@Test
	public void minimumTreeGeneratortest() {

		graph = new Graph<String>();

		initialTree();

		Kruskal<String> kruskal = new Kruskal<>(graph);		

		assertTrue(kruskal.minimumTreeGenerator().getTotalWeight() == 13);

	}

	@Test
	public void maximumTreeGeneratortest() {

		graph = new Graph<String>();

		initialTree();

		Kruskal<String> kruskal = new Kruskal<>(graph);
		
		assertTrue(kruskal.maximumTreeGenerator().getTotalWeight() == 24);
	}

	private void initialTree() {

		Vertex<String> A = new Vertex<String>("A", 1);
		Vertex<String> B = new Vertex<String>("B", 2);
		Vertex<String> C = new Vertex<String>("C", 3);
		Vertex<String> D = new Vertex<String>("D", 4);
		Vertex<String> E = new Vertex<String>("E", 5);

		graph.addVertex(A);
		graph.addVertex(B);
		graph.addVertex(C);
		graph.addVertex(D);
		graph.addVertex(E);

		graph.addConnectionWithWeight(A, D, 4);
		graph.addConnectionWithWeight(A, B, 5);
		graph.addConnectionWithWeight(B, D, 1);
		graph.addConnectionWithWeight(B, E, 2);
		graph.addConnectionWithWeight(B, C, 7);
		graph.addConnectionWithWeight(C, D, 6);
		graph.addConnectionWithWeight(C, E, 6);
		graph.addConnectionWithWeight(D, E, 3);
	}

}
