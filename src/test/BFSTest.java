package test;

import org.junit.Test;

import model.BFS;
import model.Graph;
import model.Vertex;

public class BFSTest {

	BFS<Integer> bfs = new BFS<Integer>();
	Graph<Integer> g = new Graph<Integer>();

	@Test(expected = IllegalArgumentException.class)
	public void testNull() {
		bfs.isConvex(null);
	}

	@Test
	public void reachableTwoComponentsTest() {

		Graph<Integer> g = new Graph<Integer>();

		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);
		Vertex<Integer> v3 = new Vertex<Integer>(3);
		Vertex<Integer> v4 = new Vertex<Integer>(4);
		Vertex<Integer> v5 = new Vertex<Integer>(5);

		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v4);
		g.addVertex(v5);

		g.addConnection(v1, v2);
		g.addConnection(v2, v3);
		g.addConnection(v5, v4);

		int expected[] = { 1, 2, 3 };
		Asserts.equals(expected, bfs.reachable(g, 1));
	}

	@Test
	public void reachableOneConnectionTest() {

		Graph<Integer> g = new Graph<Integer>();

		Vertex<Integer> v3 = new Vertex<Integer>(3);
		Vertex<Integer> v4 = new Vertex<Integer>(4);

		g.addVertex(v3);
		g.addVertex(v4);

		g.addConnection(v3, v4);

		int expected[] = { 3,4};
		Asserts.equals(expected, bfs.reachable(g, 3));
	}

	@Test
	public void reachableAloneTest() {

		Graph<Integer> g = new Graph<Integer>();

		Vertex<Integer> v3 = new Vertex<Integer>(3);

		g.addVertex(v3);

		int expected[] = { 3 };
		Asserts.equals(expected, bfs.reachable(g, 3));
	}

	@Test
	public void reachableConvexTest() {

		Graph<Integer> g = new Graph<Integer>();

		Vertex<Integer> v1 = new Vertex<Integer>(1);
		Vertex<Integer> v2 = new Vertex<Integer>(2);
		Vertex<Integer> v3 = new Vertex<Integer>(3);
		Vertex<Integer> v4 = new Vertex<Integer>(4);

		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v4);

		g.addConnection(v1, v2);
		g.addConnection(v2, v3);
		g.addConnection(v3, v4);

		int expected[] = { 1, 2, 3, 4 };		
		Asserts.equals(expected, bfs.reachable(g, 3));
	}

}
