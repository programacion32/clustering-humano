package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

public class Asserts {
	
	public static void equals(int[] expected, Set<Integer> getted) {
		assertEquals(expected.length, getted.size());

		for (int i = 0; i < expected.length; i++) {
			assertTrue(getted.contains(expected[i]));
		}
	}
}
