package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Graph;
import model.Vertex;

public class GraphVertexTest {

	@Test
	public void addVertexTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		g.addVertex(v1);
		assertTrue(g.existVertex(v1));
	}	

	@Test(expected = Exception.class)
	public void removeVertexTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		g.addVertex(v1);
		g.removeVertex(v1);
		g.existVertex(v1);
	}

	@Test(expected = Exception.class)
	public void notExistVertexTest() {
		Graph<Integer> g = new Graph<Integer>();
		Vertex<Integer> v1 = new Vertex<Integer>(1);
		g.existVertex(v1);
	}	

}
